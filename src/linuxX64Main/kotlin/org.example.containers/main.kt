package org.example.containers

import kotlinx.cinterop.*
import platform.posix.*
import platform.scheduler.clone_process
import platform.scheduler.wait_for_process
import kotlin.system.exitProcess

private const val IN_POS = 1
private val globalArena = Arena()
private val status = globalArena.alloc<IntVar>()
// The Pipe used to synchronize parent and child.
private val pipeFd = globalArena.allocArray<IntVar>(2)
// Use 65 KB for the stack size.
private const val STACK_SIZE = 65536uL
// New pid namespace.
private const val CLONE_NEWPID = 0x20000000
// New utsname group.
private const val CLONE_NEWUTS = 0x04000000
// New user namespace.
private const val CLONE_NEWUSER = 0x10000000

fun main() {
    val stack = malloc(STACK_SIZE)
    pipe(pipeFd)
    println("Hello from Parent! (PID: ${getpid()})")
    val childPid = clone_process(
        fn = staticCFunction(::runContainer),
        stack = stack,
        stackSize = STACK_SIZE,
        flags = CLONE_NEWPID or CLONE_NEWUTS or CLONE_NEWUSER or SIGCHLD,
        arg = null
    )
    if (childPid < 0) {
        fprintf(stderr, "Cannot clone process: ${strerror(errno)?.toKString()}\n")
        exitProcess(-1)
    }

    println("Clone PID: $childPid")
    updateMap(pid = childPid.toUInt(), newId = 1000u, updateUid = true)
    updateMap(pid = childPid.toUInt(), newId = 1000u, updateUid = false)
    // Close the write end of the pipe, to signal to the child that we have updated the UID and GID maps.
    close(pipeFd[IN_POS])
    // Wait for every child process to terminate.
    wait_for_process(status.ptr)
    println("Clone Process Status: ${status.value}")
    globalArena.clear()
    free(stack)
}

@Suppress("UNUSED_PARAMETER")
private fun runContainer(args: COpaquePointer?): Int {
    println("Hello from Child! (PID: ${getpid()})")
    // Close our descriptor for the write end of the pipe so that we see EOF when parent closes its descriptor.
    close(pipeFd[IN_POS])
    limitProcessCreation()
    setupEnvironment()
    setupRoot("./root")
    // Load the shell process.
    val rc = runProgram("/bin/sh")
    return if (rc == -1) EXIT_FAILURE else EXIT_SUCCESS
}

private fun updateMap(pid: UInt, newId: UInt, updateUid: Boolean) = memScoped {
    val value = "0 $newId 1"
    val filePath = if (updateUid) "/proc/$pid/uid_map" else "/proc/$pid/gid_map"
    val fd = open(filePath, O_RDWR)
    write(__fd = fd, value.cstr, __n = value.length.toULong())
    close(fd)
}

private fun setupRoot(dirPath: String) {
    chroot(dirPath)
    chdir("/")
}

private fun setupEnvironment() {
    clearenv()
    setenv("TERM", "xterm-256color", 0)
    setenv("PATH", "/bin/:/sbin/:usr/bin:/usr/sbin", 0)
}
