package org.example.containers

import kotlinx.cinterop.*
import platform.posix.execvp

internal fun runProgram(cmd: String, vararg args: String): Int = memScoped {
    val programArgs = if (args.isNotEmpty()) {
        allocArrayOf(arrayOf(cmd, *args).map { it.cstr.getPointer(this) } + null)
    } else {
        allocArrayOf(arrayOf(cmd).map { it.cstr.getPointer(this) } + null)
    }
    execvp(cmd, programArgs)
}
