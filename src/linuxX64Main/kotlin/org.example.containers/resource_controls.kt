package org.example.containers

import kotlinx.cinterop.cstr
import kotlinx.cinterop.memScoped
import platform.posix.*

private const val CGROUP_DIR = "/sys/fs/cgroup/pids/containers"

private fun writeRule(path: String, value: String) = memScoped {
    val file = open(path, O_WRONLY or O_APPEND )
    // Update a given file with a string value.
    write(file, value.cstr, value.length.toULong())
    close(file)
}

internal fun limitProcessCreation() {
    mkdir(CGROUP_DIR, (S_IRUSR or S_IWUSR).toUInt())
    writeRule("$CGROUP_DIR/cgroup.procs", "${getpid()}")
    // Apply a limit on the number of processes that can run.
    writeRule("$CGROUP_DIR/pids.max", "5")
    // Ensure the Linux Kernel cleans up the directory.
    writeRule("$CGROUP_DIR/notify_on_release", "1")
}
