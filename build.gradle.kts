group = "org.example"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.7.0"
}

repositories {
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("scheduler")
        }
        binaries {
            executable("containers") {
                entryPoint = "org.example.containers.main"
            }
        }
    }
}
